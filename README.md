# Gaia UI Automation Tutorials - Overview #

My implementation and repository of the [Gaia UI Automation Tutorial](https://developer.mozilla.org/en-US/Firefox_OS/Platform/Automated_testing/gaia-ui-tests/Part_1_Marionette_Firefox_OS_start) by Mozilla using Python and Marionette.

This project follows the tutorial steps with commits representing work I've done to complete the tutorials. It will also include some additional tests and ideas for testing the Contacts app in FirefoxOS (eventually).

### The Set-Up ###

* For this project, the tests are written using
    * Python 2.7.6
    * [Marionette](https://developer.mozilla.org/en-US/docs/Mozilla/QA/Marionette) 0.8.1
    * Unittest and the python interpreter as the main test runner
  
* For testing, I used
    * B2G version 33.0a1 on Ubuntu 14.04 and Windows 8.1
    
* This project is a test project. All tests may be run using 
  ```
  python test_add_contacts.py
  ```

### Allow Myself To Introduce...Myself ###

* [Josh Grant](http://twitter.com/joshin4colours), test developer extraordinaire
* I am interested in FirefoxOS from an automation/testing perspective and because it looks like a cool evolution of the mobile web!