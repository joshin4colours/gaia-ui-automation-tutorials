import unittest
import time
from marionette import By
from marionette import Wait
from marionette import Marionette

class TestContacts(unittest.TestCase):
    # locators
    _contact_frame_locator = (By.CSS_SELECTOR, "iframe[data-url*='contacts']")
    _save_button_locator = (By.ID, 'save-button')

    def unlock_screen(self):
        self.marionette.execute_script('window.wrappedJSObject.lockScreen.unlock();')

    def kill_all(self):
        self.marionette.switch_to_frame()
        self.marionette.execute_async_script("""
             // Kills all running apps, except the homescreen.
             function killAll() {
               let manager = window.wrappedJSObject.AppWindowManager;

               let apps = manager.getApps();
               for (let id in apps) {
                 let origin = apps[id].origin;
                 if (origin.indexOf('verticalhome') == -1) {
                   manager.kill(origin);
                 }
               }
             };
             killAll();
             // return true so execute_async_script knows the script is complete
             marionetteScriptFinished(true);
            """)

    def setUp(self):
        # Create the client for this session. Assuming you're using the default port on a Marionette instance running locally
        self.marionette = Marionette()
        self.marionette.start_session()

        # Unlock the screen
        self.unlock_screen()

        #kill all open apps
        self.kill_all()
		
        # Switch context to the homescreen iframe and tap on the contacts icon
        time.sleep(2)
        home_frame = self.marionette.find_element('css selector', 'div.homescreen iframe')
        self.marionette.switch_to_frame(home_frame)
			
    def tearDown(self):
        # Close the Marionette session now that the test is finished
        self.marionette.delete_session()
	
    def test_add_contacts(self):
        contacts_icon = self.marionette.find_element('xpath', "//div[@class='icon']//span[contains(text(),'Contacts')]")
        contacts_icon.tap()

        # Switch context back to the base frame
        self.marionette.switch_to_frame()
        # define the contacts iframe locator as a By instead of a pair of strings

        # Wait for the contacts frame using a dynamic wait instead of sleeps
        Wait(self.marionette).until(lambda m: m.find_element(*self._contact_frame_locator).is_displayed())

        # Switch context to the contacts app
        contacts_frame = self.marionette.find_element(*self._contact_frame_locator)
        self.marionette.switch_to_frame(contacts_frame)

        # Tap [+] to add a new Contact
        self.marionette.find_element('id', 'add-contact-button').tap()
        Wait(self.marionette).until(lambda m: m.find_element('id', 'save-button').location['y']== 0)

        # Type name into the fields
        self.marionette.find_element('id', 'givenName').send_keys('John')
        self.marionette.find_element('id', 'familyName').send_keys('Doe')

        # Tap done
        # create a By locator for the save button
        self.marionette.find_element(*self._save_button_locator).tap()
        Wait(self.marionette).until(lambda m: not m.find_element(*self._save_button_locator).is_displayed())

        # Now let's find the contact item and get its text
        contact_name = self.marionette.find_element('css selector', 'li.contact-item p').text     
        assert contact_name == 'John Doe'

if __name__ == '__main__':
    unittest.main()
